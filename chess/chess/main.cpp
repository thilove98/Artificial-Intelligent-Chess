#include <SFML/Graphics.hpp>
#include<iostream>
#include<Windows.h>
#include<string>
#include<stdio.h>
using namespace std;
using namespace sf;
const int leng = 56, X = 452, Y = 452;
struct point
{
	int x, y;
	point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};
string getpath()
{
	char s[MAX_PATH];
	return string(s, GetModuleFileName(NULL, s, MAX_PATH));
}
string toChessNote(Vector2f p)
{
	string s = "";
	s += char(p.x / leng + 97);
	s += char(7 - p.y / leng + 49);
	return s;
}
Sprite f[32];
Vector2f oldPos, newPos;
int board[8][8] =
{ -1,-2,-3,-4,-5,-3,-2,-1,
-6,-6,-6,-6,-6,-6,-6,-6,
0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0,
6, 6, 6, 6, 6, 6, 6, 6,
1, 2, 3, 4, 5, 3, 2, 1 };

void loadPosition()
{

	int k = 0;
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
		{
			int n = board[i][j];
			if (!n) continue;
			int x = abs(n) - 1;
			int y = n > 0 ? 1 : 0;
			f[k].setTextureRect(IntRect(x*leng, leng*y, leng, leng));
			f[k].setPosition(leng*j, leng*i);
			k++;
		}
}
int find(string s)
{
	for (int i = 0; i < 32; i++)if (toChessNote(Vector2f(f[i].getPosition().x, f[i].getPosition().y)) == s)return i;
	return -1;
}
bool oneSet(int i, int j)
{
	return !(i <= 15 && j >= 16 || i >= 16 && j <= 15);
}
void checkTouch(string s, int j)
{
	for (int i = 0; i < 32; i++)
	{
		Vector2f p = f[i].getPosition();
		if (toChessNote(p) == s.substr(2, 3) && i != j)
			if (!oneSet(i, j))
				f[i].setPosition(-69, -69);
	}
}
Vector2f getPosition(string s)
{
	
}
int main()
{
	RenderWindow window(VideoMode(X, Y), "Chess", Style::None | Style::Titlebar);
	string address = getpath();
	address.erase(address.size() - 9, 9);
	Texture t1, t2;
	t1.loadFromFile(address + "figures.png");
	t2.loadFromFile(address + "board0.png");
	Sprite s(t1);
	Sprite board(t2);
	for (int i = 0; i < 32; i++)f[i].setTexture(t1);
	loadPosition();
	bool isMove = false;
	float dx = 0, dy = 0;
	int n = 0;

	string str;
	int go[32] = {};
	string save = "";
	cout << int('a') << endl;
	while (window.isOpen())
	{
		Vector2i pos = Mouse::getPosition(window);
		Event e;

		while (window.pollEvent(e))
		{

			if (e.type == Event::Closed)window.close();
			//if (e.type == Event::KeyPressed&&e.key.code == Keyboard::C)window.close();
			if (e.type == Event::MouseButtonPressed)
				if (e.key.code == Mouse::Left)
					for (int i = 0; i < 32; i++)
						if (f[i].getGlobalBounds().contains(pos.x, pos.y))
						{
							isMove = true;
							n = i;
							dx = pos.x - f[i].getPosition().x;
							dy = pos.y - f[i].getPosition().y;
							oldPos = f[i].getPosition();
							go[i] = 1;
						}
			if (e.type == Event::MouseButtonReleased)
				if (e.key.code == Mouse::Left)
				{
					Vector2f p = f[n].getPosition() + Vector2f(leng / 2, leng / 2);
					Vector2f newPos = Vector2f(leng*int(p.x / leng), leng*int(p.y / leng));
					if (newPos.x<0 || newPos.y<0 || newPos.x>X - 56 || newPos.y>Y - 56)
					{
						f[n].setPosition(oldPos);
						isMove = false;
					}
					else
					{
						f[n].setPosition(newPos);
						isMove = false;
						str = toChessNote(oldPos) + toChessNote(newPos);
						if (go[n])if (toChessNote(oldPos) != toChessNote(newPos))
						{
							go[n] = 0;
							cout << str << endl;
							save = save + str;
							checkTouch(str, n);
						}
					}
				}
			if (e.type == Event::KeyReleased&&e.key.code == Keyboard::Left)
			{
				cout << "key pressed\n";
				cout << save << endl;
				string temp = save.size()>0?save.substr(save.size() - 4, 4):"";
				if (save.size() > 0)save = save.substr(0, save.size() - 4);
				cout << save +temp<< endl;
			}

		}

		if (isMove) f[n].setPosition(pos.x - dx, pos.y - dy);
		window.clear();
		window.draw(board);
		for (int i = 0; i < 32; i++)window.draw(f[i]);

		window.display();
	}
	return 0;
}
